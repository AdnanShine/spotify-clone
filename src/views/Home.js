import Section from "components/Section";
import songs from "data/songs"

function Home() {

	return (
		<div className="grid gap-y-8">
			<Section
				title="Focus"
				items={songs}
			/>
			<Section
				title="Spotify Playlist"
				items={songs}
			/>
			<Section
				title="Made for you"
				items={songs}
			/>
		</div>
	)
}

export default Home
