export default [
	{
	  id: 4,
	  title: 'Chasing Dreams - Soundscapes OST',
	  description: 'Original Soundtrack for Dreamers',
	  artist: 'Skywalker',
	  image: 'https://i.scdn.co/image/ab67706c0000da84fcb8b92f2615d3261b8eb146',
	  type: 'album',
	  src: 'https://freesound.org/data/previews/612/612095_5674468-lq.mp3',
	},
	{
	  id: 1,
	  title: 'Serenity of Solitude',
	  artist: 'Zen Master',
	  description: 'Embrace peace and tranquility with soothing melodies',
	  image: 'https://i.scdn.co/image/ab67706f00000002ca5a7517156021292e5663a6',
	  type: 'album',
	  src: 'https://freesound.org/data/previews/612/612092_7037-lq.mp3',
	},
	{
	  id: 5,
	  title: 'Journey Through Time',
	  artist: 'Wanderer',
	  description: 'Embark on a musical voyage through time and space',
	  image: 'https://i.scdn.co/image/ab67616100005174802686196d39eb0b7b5cd8b1',
	  type: 'artist',
	  src: 'https://freesound.org/data/previews/612/612087_1648170-lq.mp3',
	},
	{
	  id: 2,
	  title: 'Mindful Moments - Soundscapes OST',
	  artist: 'Mindfulness Guru',
	  description: 'Original Soundtrack for Mindful Living',
	  image: 'https://i.scdn.co/image/d39397d2f6a1525b3fe90369c89ea2d94aac5714',
	  type: 'podcast',
	  src: 'https://freesound.org/data/previews/612/612085_28867-lq.mp3',
	},
	{
	  id: 3,
	  title: 'Uncharted Horizons - Soundscapes OST',
	  artist: 'Explorer',
	  description: 'Discover uncharted territories with captivating melodies',
	  image: 'https://i.scdn.co/image/ab67706c0000da84fcb8b92f2615d3261b8eb146',
	  type: 'album',
	  src: 'https://www.soundhelix.com/examples/mp3/SoundHelix-Song-2.mp3',
	},
  ];
  