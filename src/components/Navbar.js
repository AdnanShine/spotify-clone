import Navigation from "./Navbar/Navigation";

function Navbar() {
  return (
    <nav className="h-[3.75rem] flex items-center justify-between px-8 relative z-10">
      <Navigation />

      <div className="flex">
        <button className="h-8 p-5 flex items-center justify-center text-white rounded-full">
          Log in
        </button>

        <button className="h-8 p-5 flex items-center justify-center text-black rounded-full bg-white">
          Sign Up
        </button>
      </div>
    </nav>
  );
}

export default Navbar;
