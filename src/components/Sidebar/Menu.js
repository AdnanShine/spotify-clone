import { Icon } from "Icons";
import { NavLink } from "react-router-dom";

function Menu() {
  return (
    <nav className="px-1">
      <div className="bg-background-base p-1 py-2 rounded-lg cursor-default p-10">
        <ul className="flex flex-col">
          <li>
            <NavLink
              exact
              to={"/"}
              className="h-10 flex text-white gap-x-4 items-center text-sm font-semibold text-link rounded hover:text-white px-4"
            >
              <span>
                <Icon name="home" />
              </span>
              Home
            </NavLink>
          </li>
          <li>
            <NavLink to="/" className="h-10 flex gap-x-4 items-center text-sm font-semibold text-link rounded hover:text-white px-4">
              <span>
                <Icon name="search" />
              </span>
              Search
            </NavLink>
          </li>
        </ul>
      </div>
    </nav>
  );
}

export default Menu;
