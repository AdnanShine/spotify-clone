import { Icon } from "Icons";
import { NavLink } from "react-router-dom";

function SubMenu() {
  return (
    <nav className="px-1">
      <div className="bg-background-base p-1 py-2 rounded-lg cursor-default p-10">
        <ul className="flex flex-col">
          <li className="py-2">
            <div className="background-elevated-base py-5 px-2 rounded-lg cursor-default">
              <span className="text-1xl text-white font-semibold tracking-tight">Create your first playlist</span>
              <br />
              <span className="text-sm">It's easy, we'll help you</span>
              <button className="h-8 p-5 flex items-center justify-center text-black bg-white rounded-full">
                Create Playlist
              </button>
            </div>
          </li>
          <li>
            <div className="background-elevated-base py-5 px-2 rounded-lg cursor-default">
              <span className="text-1xl text-white font-semibold tracking-tight">Let's find some podcasts to follow</span>
              <br />
              <span className="text-sm">We'll keep you updated on new episodes</span>
              <button className="h-8 p-5 flex items-center justify-center text-black bg-white rounded-full">
                Browse podcast
              </button>
            </div>
          </li>
        </ul>
      </div>
    </nav>
  );
}

export default SubMenu;
