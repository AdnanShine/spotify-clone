import { Icon } from "Icons";
import Menu from "components/Sidebar/Menu";
import { useSelector } from "react-redux";
import SubMenu from "./Sidebar/SubMenu";

function Sidebar() {
  return (
    <aside className="px-2 w-60 pt-1 flex flex-shrink-0 flex-col bg-black">
      <Menu />
      <nav className="mt-2 px-1">
        <div className="bg-background-base p-1 rounded-lg cursor-default">
          <ul>
            <li>
              <a
                href="#"
                className="py-2 px-6 flex items-center justify-between group text-sm text-link font-semibold hover:text-white"
              >
                <div className="flex">
                  <span className="w-6 h-6 flex items-center justify-center mr-4 bg-opacity-60 rounded-sm text-white">
                    <Icon name="musiclibrary" size={12} />
                  </span>
                  Your Library
                </div>
                <span className="w-6 h-6 flex items-center justify-center mr-4 bg-opacity-60 rounded-sm text-white">
                  <Icon name="plus" size={12} />
                </span>
              </a>
            </li>
            <li>
              <SubMenu />
            </li>
          </ul>
        </div>
      </nav>
    </aside>
  );
}

export default Sidebar;
