import { NavLink } from "react-router-dom";

function Title({ title, more = false }) {
  return (
    <header className="flex items-center justify-between mb-4">
      <NavLink to="#">
        <h3 className="text-2xl text-white font-semibold tracking-tight hover:underline">
          {title}
        </h3>
      </NavLink>
      <NavLink
        className={
          "text-xs hover:underline font-semibold uppercase text-link tracking-wider"
        }
        to={"/"}
      >
        SEE ALL
      </NavLink>
    </header>
  );
}

export default Title;
