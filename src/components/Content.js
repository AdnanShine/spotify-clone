import Navbar from "components/Navbar";
import Home from "views/Home";

function Content() {
  return (
    <main className="bg-background-base p-1 rounded-lg cursor-default flex-auto overflow-auto py-2">
      <Navbar />
      <div className="px-8 py-5">
        <Home />
      </div>
    </main>
  );
}

export default Content;
